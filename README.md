This is a fork of https://GitHub.com/JosXa/BotListBot which is licensed under the MIT License.

## Installation Instruction

- For some reason. the `setup.sh` did not work for me.

`virtualenv -p /usr/bin/python3 venv`

`. ./venv/bin/activate`

`pip3 install -r requirements.txt`

Edit settings.py with appropriate values.

##### Create database
(to be done inside virtualenv)

`python3`
Copy the appropriate contents in `./model/__init__.py`


Try running the bot using the command,

DEV=True python3 bot.py "TOKEN"


There will still be errors, need to handle that. 
